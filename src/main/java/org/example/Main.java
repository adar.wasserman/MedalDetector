package org.example;

import nu.pattern.OpenCV;
import org.opencv.core.*;
import org.opencv.imgcodecs.Imgcodecs;
import org.opencv.imgproc.Imgproc;
import org.opencv.videoio.VideoCapture;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.util.List;

public class Main {
    public static void main(String[] args) throws Exception {
        OpenCV.loadShared();
        System.loadLibrary(Core.NATIVE_LIBRARY_NAME); //Load OpenCV native library

        Mat mat = new Mat(); //create an image matrix to store the image.

        VideoCapture videoDevice = new VideoCapture();

        videoDevice.open(0); //open the camera at index 0

        if (videoDevice.isOpened()) { //check if the camera has opened.
            videoDevice.read(mat); // Get frame from camera

            System.out.println(mat); //print the matrix we got from the camera.

            videoDevice.release(); //release the camera
        } else {
            System.out.println("Error."); // exit if error has occurred.
            System.exit(1);
            return;
        }


        BufferedImage img = matToBufferedImage(processImage(mat)); // process the image and convert it to a BufferedImage
        ImageIO.write(img, "jpg", new File("out7.jpg")); // write the BufferedImage to a file named out7.jpg.
    }


    static BufferedImage matToBufferedImage(Mat matrix)throws Exception {
        MatOfByte mob=new MatOfByte();
        Imgcodecs.imencode(".jpg", matrix, mob);
        byte ba[]=mob.toArray();
        BufferedImage bi= ImageIO.read(new ByteArrayInputStream(ba));
        return bi;
    }

    static Mat processImage(Mat input){
        GripPipeline pipeline = new GripPipeline(); //create an instance of GripPipeline.
        List<MatOfPoint> contours = pipeline.process(input); //process the image to get the contours.
        for (MatOfPoint matOfPoint : contours){ //iterate the contours
            Point[] targetMat = matOfPoint.toArray(); //convert the point matrix into a Point Array

            Point[] smallestAndLargest = getSmallestAndLargest(targetMat); //get the largest point and the smallest point on the matrix.
            Point smallest = smallestAndLargest[0];
            Point largest = smallestAndLargest[1];

            Imgproc.circle (
                    input,
                    new Point((smallest.x+largest.x)/2, (smallest.y+largest.y)/2), // find the center of the contours using the smallest and largest points.
                    (int) Math.max(largest.x-smallest.x, largest.y-smallest.y), // find the size of the contour.
                    new Scalar(255, 0, 0), // Color of the circle, NOTE: use BGR instead of RGB.
                    10 //thickness of 10 pixels.
            );
        }

        return input;
    }

    public static Point[] getSmallestAndLargest(Point[] a){
        Point smallest = a[0]; // create a variable called smallest that store the first point in the given array.
        Point largest = new Point(); // create an empty point.
        for (Point p : a){ //iterate the points in the given array.
            smallest.x = Math.min(p.x, smallest.x); // if the x-axis of the point is smaller than the current value of 'smallest' on the x-axis then 'smallest' on the x-axis is set to the x-axis of the point, and vice-versa.
            smallest.y = Math.min(p.y, smallest.y); // if the y-axis of the point is smaller than the current value of 'smallest' on the y-axis then 'smallest' on the y-axis is set to the y-axis of the point, and vice-versa.
            largest.x = Math.max(p.x, largest.x); // if the x-axis of the point is bigger than the current value of 'largest' on the x-axis then 'largest' on the x-axis is set to the x-axis of the point, and vice-versa.
            largest.y = Math.max(p.y, largest.y); // if the y-axis of the point is bigger than the current value of 'largest' on the y-axis then 'largest' on the y-axis is set to the y-axis of the point, and vice-versa.
        }
        return new Point[]{smallest,largest}; // return the smallest and biggest numbers on the given array. the returned value is a point array where the first element is the smallest number and second element is the biggest number.
    }

}